# BadWolf
Minimalist and privacy-oriented WebKitGTK+ browser
```
Copyright © 2019 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
SPDX-License-Identifier: BSD-3-Clause
```

The name is a reference to BBC’s Doctor Who Tv serie, I took it simply because I wanted to have a specie in the name, like some other web browsers do, but doesn’t go into the “gentle” zone.

## Differencies
Comparing from other small WebKit browsers for unixes found in the wild:

- Independent of environment, should just work if GTK and WebKitGTK does
- Static UI, no element should be added at runtime, this is to avoid potential tracking via viewport changes
- Small codebase, right now we are well under 1 000 lines
- Does not use modal editing (from vi) as that was designed for editing, not browsing
- UTF-8 encoding by default

Motivation from other clients <https://hacktivis.me/articles/www-client%20are%20broken>

## Repositories
### git
- Main: <https://hacktivis.me/git/badwolf/>, <git://hacktivis.me/git/badwolf.git>
- Mirror: <https://gitlab.com/lanodan/badWolf.git>, this one can also be used if you prefer tickets/PRs over emails

### release tarballs
- Main: <https://hacktivis.me/releases/>
- Mirror: <https://gitlab.com/lanodan/badWolf/tags>

Files ending in `.sig` are OpenPGP signatures done with my [key](https://hacktivis.me/key.asc)(`DDC9 237C 14CF 6F4D D847  F6B3 90D9 3ACC FEFF 61AE`).

## Manual Installation
Dependencies are:
- [WebKitGTK](https://webkitgtk.org/), only the latest stable will be fully supported
- C11 Compiler (such as clang or gcc)
- POSIX make (works with GNU or BSD)
- A pkg-config implementation (pkg-config and pkgconf are supported)

Compilation is done with `make`, install with `make install`.

## Notes
Most of the privacy/security stuff will be done with patches against WebKit as quite a lot isn’t into [WebKitSettings](https://webkitgtk.org/reference/webkit2gtk/stable/WebKitSettings.html) and with a generic WebKit extension that should be resuseable.
