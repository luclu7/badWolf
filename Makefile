.POSIX:

PREFIX = /usr/local

DEPS = gtk+-3.0 webkit2gtk-4.0
OBJS = badwolf

CC      = cc
CFLAGS  = -g -Wall -Wextra
CDEPS   = `pkg-config --cflags $(DEPS)`
LIBS    = `pkg-config --libs $(DEPS)`

all: $(OBJS)

.c:
	$(CC) -std=c11 $(CFLAGS) $(CDEPS) -o $@ $< $(LDFLAGS) $(LIBS)

install: badwolf
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -p badwolf $(DESTDIR)$(PREFIX)/bin/badwolf

clean:
	rm $(OBJS)

format: *.c *.h
	clang-format -style=file -assume-filename=.clang-format -i *.c *.h

.PHONY: clean install
